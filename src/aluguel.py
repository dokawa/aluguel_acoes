import collections
import json
from string import ascii_lowercase

from bs4 import BeautifulSoup
from pip._vendor import requests
from datetime import date
from dateutil import relativedelta

LIST_URL_PATTERN = "http://www.b3.com.br/pt_br/produtos-e-servicos/emprestimo-de-ativos/renda-variavel/s_empreg_ativos/renda-variavel.htm?nome=%s&dataConsulta=%s&f=0"
COMPANY_URL_PATTERN = "http://www.b3.com.br/pt_br/produtos-e-servicos/emprestimo-de-ativos/renda-variavel/s_empreg_ativos/renda-variavel-8AA8D0CD701B61040170400B64840F04.htm?empresaEmissora=%s&data=%s&f=0"

def get_last_month_date():
    today = date.today() - relativedelta.relativedelta(days=1)
    return today.strftime("%d/%m/%Y")


def get_aluguel():
    hash_map = {}
    for c in ascii_lowercase.upper():
        url = LIST_URL_PATTERN % (c, get_last_month_date())
        html = requests.get(url)
        page_html = BeautifulSoup(html.content, features="html.parser")
        category_list = page_html.find_all('tr')
        if category_list:
            print("Dropped:", category_list.pop(0))
        for element in category_list:
            name = element.get_text()
            print(element.get_text())
            name = element.get_text().replace(' ', '%20')
            nested_html = requests.get(
                COMPANY_URL_PATTERN % (name, get_last_month_date()))
            parsed_nested_html = BeautifulSoup(nested_html.content)
            nested_list = parsed_nested_html.find('tbody')
            hash_map = { **hash_map, **parse_nested_list(nested_list) }
            print(hash_map)



    file = open('./aluguel.json', "w+")
    file.write(str(hash_map).replace('\'', '\"'))
            # FileManager().get_write_file('json')
            # print("nnlist", nested_nested_list)

            # for neighborhood in nested_list:
            #     print("\t\t", neighborhood.get_text())

def parse_nested_list(nested_list):
    hash_map = {}
    if not nested_list == None:
        for line in nested_list:
            counter = 0
            for column in line:
                if counter % 6 == 0:
                    # has_map{name : c}
                    if counter == 0:
                        name = str(column).replace('<td>', '').replace('</td>', '')
                    else:
                        value = str(column).replace('<td class="text-right">', '').replace('</td>', '').replace('%',
                                                                                                                '').replace(
                            ',', '.')
                    print("%s%s" % (counter, column))
                counter += 1
            hash_map[name] = float(value)

    return hash_map

def list_aluguel():
    file = open('./aluguel.json')
    text = json.load(file)
    print(text)
    has_map = {}
    for key, value in text.items():
        has_map[key] = value

    print(has_map.items())
    sortvalue = lambda i: float(i[1])
    # sortkey = lambda i: [int(e) for e in i[0].split(':')]
    ordered_has_map = collections.OrderedDict(sorted(has_map.items(), key=sortvalue, reverse=True))
    print(ordered_has_map)

    for key, value in ordered_has_map.items():
        print("%s %s" % (key, value))




#get_aluguel()
list_aluguel()


